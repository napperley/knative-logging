package org.digieng.knativeLogging

import org.digieng.knativeLogging.logAppender.ConsoleAppender
import org.digieng.knativeLogging.logAppender.LogAppender

/** Covers logging for an area of a program/library. */
public class Logger(public val tag: String, public val logAppenders: Array<LogAppender>) : Closable {
    private var _isOpen = true
    public val isOpen: Boolean
        get() = _isOpen

    public fun logDebug(content: String) {
        logAppenders.forEach { it.logMessage(tag = tag, logLevel = LogLevel.DEBUG, content = content) }
    }

    public fun logInfo(content: String) {
        logAppenders.forEach { it.logMessage(tag = tag, logLevel = LogLevel.INFO, content = content) }
    }

    public fun logWarn(content: String) {
        logAppenders.forEach { it.logMessage(tag = tag, logLevel = LogLevel.WARN, content = content) }
    }

    public fun logError(content: String) {
        logAppenders.forEach { it.logMessage(tag = tag, logLevel = LogLevel.ERROR, content = content) }
    }

    override fun close() {
        if (_isOpen) {
            logAppenders.forEach { if (it is Closable) it.close() }
            _isOpen = false
        }
    }
}

/** A convenience function for creating a new Logger instance. */
public fun logger(tag: String, logAppenders: Array<LogAppender> = arrayOf(ConsoleAppender())): Logger =
    Logger(tag, logAppenders)

internal expect fun createTimestampString(): String
