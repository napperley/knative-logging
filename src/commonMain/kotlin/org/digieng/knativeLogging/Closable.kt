package org.digieng.knativeLogging

public interface Closable {
    public fun close()
}
