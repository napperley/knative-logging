package org.digieng.knativeLogging.logAppender

import org.digieng.knativeLogging.LogLevel

/** Base interface for a place where messages are logged. */
public interface LogAppender {
    public val includeTag: Boolean
    public val minLogLevel: LogLevel

    /**
     * Logs a message.
     * @param tag The tag to use.
     * @param logLevel Logging level to use.
     * @param content A String containing what to include in the log message.
     */
    public fun logMessage(tag: String, logLevel: LogLevel, content: String)
}
