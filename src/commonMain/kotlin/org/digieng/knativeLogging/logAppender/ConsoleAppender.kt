package org.digieng.knativeLogging.logAppender

import org.digieng.knativeLogging.LogLevel

/** Handles logging to the console. */
public expect class ConsoleAppender(
    minLogLevel: LogLevel = LogLevel.DEBUG,
    includeTag: Boolean = true
) : LogAppender
