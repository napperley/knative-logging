package org.digieng.knativeLogging

/** Contains the various levels that can be used in a log message. */
public enum class LogLevel {
    DEBUG,
    INFO,
    WARN,
    ERROR
}
