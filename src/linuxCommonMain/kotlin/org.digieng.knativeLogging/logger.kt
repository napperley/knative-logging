package org.digieng.knativeLogging

import kotlinx.cinterop.*
import platform.posix.localtime
import platform.posix.time
import platform.posix.time_tVar
import platform.posix.tm

@OptIn(UnsafeNumber::class)
internal actual fun createTimestampString(): String = memScoped {
    val timeVar = alloc<time_tVar>()
    // Initialise timeVar.
    time(timeVar.ptr)
    // Fetch the local date/time.
    val timeInfo = localtime(timeVar.ptr)
    var time = ""
    var date = ""
    if (timeInfo != null) {
        val tm = timeInfo[0]
        time = "${tm.hourStr}:${tm.minStr}:${tm.secStr}"
        date = "${tm.dayStr}-${tm.monthStr}-${tm.yearStr}"
    }
    if (time.isNotEmpty() && date.isNotEmpty()) "$date $time" else ""
}

private val tm.hourStr
    get() = "${if (tm_hour < 10) "0" else ""}$tm_hour"
private val tm.minStr
    get() = "${if (tm_min < 10) "0" else ""}$tm_min"
private val tm.secStr
    get() = "${if (tm_sec < 10) "0" else ""}$tm_sec"
private val tm.dayStr
    get() = "${if (tm_mday < 10) "0" else ""}$tm_mday"
private val tm.monthStr
    get() = "${if ((tm_mon + 1) < 10) "0" else ""}${tm_mon + 1}"
private val tm.yearStr
    get() = "${tm_year + 1900}"
