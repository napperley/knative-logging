package org.digieng.knativeLogging.logAppender

import org.digieng.knativeLogging.LogLevel
import org.digieng.knativeLogging.createTimestampString
import platform.posix.fprintf
import platform.posix.stderr

public actual class ConsoleAppender actual constructor(
    override val minLogLevel: LogLevel,
    override val includeTag: Boolean
) : LogAppender {
    override fun logMessage(tag: String, logLevel: LogLevel, content: String) {
        val msg = "[${createTimestampString()}] ${logLevel.name} - ${if (includeTag) "$tag: " else ""}$content"
        if (logLevel.ordinal >= minLogLevel.ordinal && logLevel != LogLevel.ERROR) println(msg)
        else if (logLevel.ordinal >= minLogLevel.ordinal && logLevel == LogLevel.ERROR) fprintf(stderr, "$msg\n")
    }
}
