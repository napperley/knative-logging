package org.digieng.knativeLogging.logAppender

import kotlinx.cinterop.*
import org.digieng.knativeLogging.Closable
import org.digieng.knativeLogging.LogLevel
import org.digieng.knativeLogging.createTimestampString
import platform.posix.fclose
import platform.posix.fopen
import platform.posix.fwrite
import platform.posix.stat

/** Handles logging to a file. */
@Suppress("UnnecessaryOptInAnnotation")
@OptIn(UnsafeNumber::class)
public class FileAppender(
    override val includeTag: Boolean,
    override val minLogLevel: LogLevel,
    @Suppress("MemberVisibilityCanBePrivate")
    public val logFilePath: String,
    /** Maximum log file size in KB. */
    @Suppress("MemberVisibilityCanBePrivate")
    public val maxLogFileSize: ULong = 800uL
) : LogAppender, Closable {
    private var logFile = fopen(logFilePath, "a")
    public val isOpen: Boolean
        get() = logFile != null

    override fun logMessage(tag: String, logLevel: LogLevel, content: String): Unit = memScoped {
        val msg = "[${createTimestampString()}] ${logLevel.name} ${if (includeTag) "- $tag: " else ""}$content\n"
        val msgCstr = msg.cstr
        if (maxFileSizeExceeded()) clearFile()
        if (logFile != null) {
            fwrite(__s = logFile, __n = 1u, __ptr = msgCstr, __size = msg.length.toUInt())
        }
    }

    private fun clearFile() {
        close()
        // Create an empty file.
        logFile = fopen(logFilePath, "w")
        close()
        logFile = fopen(logFilePath, "a")
    }

    override fun close() {
        if (logFile != null) {
            fclose(logFile)
            logFile = null
        }
    }

    private fun maxFileSizeExceeded(): Boolean {
        val tmp = fileSize()
        // Convert the file size to KB.
        val size = (if (tmp <= 1000) 1 else tmp / 1000).toULong()
        return size > maxLogFileSize
    }

    private fun fileSize(): Long = memScoped {
        val fileStats = alloc<stat>()
        stat(logFilePath, fileStats.ptr)
        // Return the file size in bytes.
        fileStats.st_size.toLong()
    }
}
