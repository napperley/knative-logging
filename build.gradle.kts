import org.jetbrains.dokka.gradle.DokkaTask

group = "org.digieng"
version = "0.3.0"

plugins {
    kotlin("multiplatform") version "1.6.20"
    id("maven-publish")
    id("org.jetbrains.dokka") version "1.6.10"
}

repositories {
    mavenCentral()
    jcenter()
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadocJar by tasks.register("javadocJar", Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

kotlin {
    explicitApi()
    linuxX64()
    linuxArm32Hfp("linuxArm32")

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.6.20"
                implementation(kotlin("stdlib-common", kotlinVer))
            }
        }

        val commonMain by getting
        val linuxCommonMain by creating { dependsOn(commonMain) }
        @Suppress("UNUSED_VARIABLE")
        val linuxX64Main by getting { dependsOn(linuxCommonMain) }
        @Suppress("UNUSED_VARIABLE")
        val linuxArm32Main by getting { dependsOn(linuxCommonMain) }

        all {
            languageSettings.optIn("kotlin.RequiresOptIn")
        }
    }
}

publishing {
    publications.withType<MavenPublication> {
        artifact(javadocJar)
    }
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0
