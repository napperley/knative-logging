rootProject.name = "knative-logging"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
    }
}
